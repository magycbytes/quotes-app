package com.magycbytes.quotesapp;

import android.app.Application;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.magycbytes.quotesapp.database.QuotesSqlHelper;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Alex on 22/12/2016.
 */

public class QuotesApp extends Application {

    public static QuotesSqlHelper sQuotesSqlHelper;

    @Override
    public void onCreate() {
        super.onCreate();

        // Set up Crashlytics, disabled for debug builds
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build();

        // Initialize Fabric with the debug-disabled crashlytics.
        Fabric.with(this, crashlyticsKit);

        sQuotesSqlHelper = new QuotesSqlHelper(this);
    }

    @Override
    public void onTerminate() {
        if (sQuotesSqlHelper != null) {
            sQuotesSqlHelper.close();
            sQuotesSqlHelper = null;
        }
        super.onTerminate();
    }
}
