package com.magycbytes.quotesapp.network;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by alexandru on 29/07/16.
 */
public interface QuotesService {

    @GET("/quotes/favorites.html")
    Call<ResponseBody> getAllAuthors();

    @GET
    Call<ResponseBody> getQuotes(@Url String url);
}
