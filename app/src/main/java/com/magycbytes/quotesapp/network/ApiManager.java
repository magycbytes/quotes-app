package com.magycbytes.quotesapp.network;

import retrofit2.Retrofit;

/**
 * Created by alexandru on 29/07/16.
 */
public class ApiManager {

    public static final String BASE_URL = "https://www.brainyquote.com";


    public static QuotesService getApi() {
        return getBaseApi(BASE_URL);
    }


    private static QuotesService getBaseApi(String baseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .build();
        return retrofit.create(QuotesService.class);
    }
}
