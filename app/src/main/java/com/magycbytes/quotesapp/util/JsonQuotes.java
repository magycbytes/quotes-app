package com.magycbytes.quotesapp.util;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.magycbytes.quotesapp.R;
import com.magycbytes.quotesapp.stories.quotesExtractor.Quote;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import static com.magycbytes.quotesapp.R.raw.quotes;

/**
 * Created by Alex on 21/12/2016.
 */

public class JsonQuotes {

    private List<Integer> mFilesIds = new ArrayList<>();
    private Context mContext;

    public JsonQuotes(Context context) {
        mContext = context;

        mFilesIds.add(quotes);
        mFilesIds.add(R.raw.quotes_2);
        mFilesIds.add(R.raw.quotes_3);
        mFilesIds.add(R.raw.quotes_4);
        mFilesIds.add(R.raw.quotes_5);
        mFilesIds.add(R.raw.quotes_6);
        mFilesIds.add(R.raw.quotes_7);
    }

    public List<Quote> getAllQuotes() {
        List<Quote> quotesList = new ArrayList<>(12000);
        for (int i = 0; i < getProvidersSize(); ++i) {
            quotesList.addAll(deserializeQuotes(i));
        }

        return quotesList;
    }

    public List<Quote> getAuthorsQuotes(String authorLink) {
        JsonQuotes jsonQuotes = new JsonQuotes(mContext);
        for (int i = 0; i < jsonQuotes.getProvidersSize(); ++i) {
            List<Quote> quotes = jsonQuotes.deserializeQuotes(i);
            List<Quote> authorQuotes = findAuthorQuotes(quotes, authorLink);
            if (!authorQuotes.isEmpty()) return authorQuotes;
        }
        return new ArrayList<>();
    }

    public List<Quote> deserializeQuotes(int fileIndex) {
        InputStream raw = mContext.getResources().openRawResource(mFilesIds.get(fileIndex));
        Reader rd = new BufferedReader(new InputStreamReader(raw));
        return new Gson().fromJson(rd, new TypeToken<ArrayList<Quote>>() {
        }.getType());
    }

    public int getProvidersSize() {
        return mFilesIds.size();
    }

    private List<Quote> findAuthorQuotes(List<Quote> quotes, String authorLink) {

        List<Quote> authorsQuotes = new ArrayList<>();
        for (Quote quote : quotes) {
            if (quote.getAuthorLink().equals(authorLink)) {
                authorsQuotes.add(quote);
            }
        }

        return authorsQuotes;
    }
}
