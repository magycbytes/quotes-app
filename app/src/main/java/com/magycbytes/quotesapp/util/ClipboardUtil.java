package com.magycbytes.quotesapp.util;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;
import android.widget.Toast;

/**
 * Created by Alex on 22/12/2016.
 */

public class ClipboardUtil {

    public static void copyText(Context context, String text) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clipData;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            clipData = ClipData.newHtmlText("Quote", text, text);
        } else {
            clipData = ClipData.newPlainText("Quote", text);
        }

        clipboard.setPrimaryClip(clipData);

        Toast.makeText(context, "Quote copied", Toast.LENGTH_SHORT).show();
    }
}
