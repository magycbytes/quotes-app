package com.magycbytes.quotesapp.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Alex on 22/12/2016.
 */

public class ApplicationSettings {

    private static final String IS_DATA_DESERIALIZE = "IsDataDeserialize";
    private final SharedPreferences mPreferences;

    public ApplicationSettings(Context context) {
        mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public boolean isDataDeserialized() {
        return mPreferences.getBoolean(IS_DATA_DESERIALIZE, false);
    }

    public void dataWasDeserialized() {
        mPreferences.edit().putBoolean(IS_DATA_DESERIALIZE, true).apply();
    }

}
