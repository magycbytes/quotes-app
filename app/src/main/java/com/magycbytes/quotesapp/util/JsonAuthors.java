package com.magycbytes.quotesapp.util;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.magycbytes.quotesapp.R;
import com.magycbytes.quotesapp.stories.authorsExtractor.Author;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 21/12/2016.
 */

public class JsonAuthors {

    public static List<Author> getAuthors(Context context) {
        InputStream raw =  context.getResources().openRawResource(R.raw.authors);
        Reader rd = new BufferedReader(new InputStreamReader(raw));
        return new Gson().fromJson(rd, new TypeToken<ArrayList<Author>>(){}.getType());
    }
}
