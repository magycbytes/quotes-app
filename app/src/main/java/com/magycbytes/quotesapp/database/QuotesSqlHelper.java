package com.magycbytes.quotesapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Alex on 22/12/2016.
 */

public class QuotesSqlHelper extends SQLiteOpenHelper {

    private static final String LOG_TAG = QuotesSqlHelper.class.getSimpleName();
    private static final String DATABASE_NAME = "QuotesDatabase.db";
    private static final int DATABASE_VERSION = 1;

    public QuotesSqlHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d(LOG_TAG, "Creating database");

        AuthorsTable authorsTable = new AuthorsTable();
        QuotesTable quotesTable = new QuotesTable();
        TagsTable tagsTable = new TagsTable();

        sqLiteDatabase.execSQL(authorsTable.getCreateStatement());
        sqLiteDatabase.execSQL(quotesTable.getCreateStatement());
        sqLiteDatabase.execSQL(tagsTable.getCreateStatement());
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        Log.d(LOG_TAG, "Upgrading database from " + i + " to " + i1);
    }
}
