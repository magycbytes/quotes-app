package com.magycbytes.quotesapp.database;

import android.content.ContentValues;
import android.database.Cursor;

import com.magycbytes.quotesapp.QuotesApp;
import com.magycbytes.quotesapp.stories.authorsExtractor.Author;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 22/12/2016.
 */

public class AuthorsTable extends DatabaseTable {

    public static final String NAME = "Name";
    public static final String TABLE_NAME = "Authors";
    public static final String AUTHORS_ID = "AuthorsId";

    @Override
    public String getCreateStatement() {
        addTableName(TABLE_NAME);
        addPrimaryKey(AUTHORS_ID);
        addString(NAME, 64);

        return getResultQuery();
    }

    @Override
    public void delete() {
        QuotesApp.sQuotesSqlHelper.getWritableDatabase().delete(TABLE_NAME, null, null);
    }

    public long saveAuthor(String authorName) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME, authorName);

        return QuotesApp.sQuotesSqlHelper.getWritableDatabase().insert(TABLE_NAME, null, contentValues);
    }

    public List<Author> getAll() {
        Cursor cursor = QuotesApp.sQuotesSqlHelper.getReadableDatabase().query(
                TABLE_NAME,
                new String[]{NAME, AUTHORS_ID},
                null,
                null,
                null,
                null,
                null);

        List<Author> authors = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Author author = new Author(cursor.getString(0), cursor.getLong(1));
                authors.add(author);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return authors;
    }
}
