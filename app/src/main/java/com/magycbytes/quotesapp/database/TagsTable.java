package com.magycbytes.quotesapp.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.magycbytes.quotesapp.QuotesApp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Alex on 22/12/2016.
 */

public class TagsTable extends DatabaseTable {

    public static final String QUOTE_ID = "QuoteId";
    public static final String TAG_NAME = "TagName";
    public static final String TABLE_NAME = "Tags";

    @Override
    public String getCreateStatement() {
        addTableName(TABLE_NAME);
        addPrimaryKey("TagId");
        addIntColumn(QUOTE_ID);
        addString(TAG_NAME, 64);

        return getResultQuery();
    }

    @Override
    public void delete() {
        QuotesApp.sQuotesSqlHelper.getWritableDatabase().delete(TABLE_NAME, null, null);
    }

    public void save(long quoteId, List<String> tags) {

        SQLiteDatabase database = QuotesApp.sQuotesSqlHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        for (String tag : tags) {
            contentValues.clear();
            contentValues.put(QUOTE_ID, quoteId);
            contentValues.put(TAG_NAME, tag);

            database.insert(TABLE_NAME, null, contentValues);
        }
    }

    public List<String> getDistinctTags() {
        Cursor cursor = QuotesApp.sQuotesSqlHelper.getReadableDatabase().query(
                true,
                TABLE_NAME,
                new String[]{TAG_NAME},
                null,
                null,
                null,
                null,
                null,
                null
        );

        List<String> distinctTags =  new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                distinctTags.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        cursor.close();

        Collections.sort(distinctTags);
        return distinctTags;
    }

    public List<String> getTags(long quoteId) {
        Cursor cursor = QuotesApp.sQuotesSqlHelper.getReadableDatabase().query(
                TABLE_NAME,
                new String[]{TAG_NAME},
                QUOTE_ID + " LIKE ?",
                new String[]{String.valueOf(quoteId)},
                null,
                null,
                null
        );

        List<String> tags = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                tags.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        cursor.close();

        return tags;
    }
}
