package com.magycbytes.quotesapp.database;

/**
 * Created by Alex on 22/12/2016.
 */

public abstract class DatabaseTable {

    private StringBuilder mBuilder = new StringBuilder();

    void addTableName(String tableName) {
        mBuilder.append("CREATE TABLE IF NOT EXISTS ")
                .append(tableName)
                .append(" (");
    }

    void addPrimaryKey(String columnName) {
        mBuilder.append(columnName)
                .append(" INTEGER PRIMARY KEY AUTOINCREMENT");
    }

    void addIntColumn(String columnName) {
        mBuilder.append(", ")
                .append(columnName)
                .append(" INTEGER");
    }

    void addString(String columnName, int maxLength) {
        mBuilder.append(", ")
                .append(columnName)
                .append(" VARCHAR(")
                .append(maxLength)
                .append(")");
    }

    String getResultQuery() {
        endQuery();
        return mBuilder.toString();
    }

    private void endQuery() {
        mBuilder.append(");");
    }

    public abstract String getCreateStatement();

    public abstract void delete();
}
