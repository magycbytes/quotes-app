package com.magycbytes.quotesapp.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.magycbytes.quotesapp.QuotesApp;
import com.magycbytes.quotesapp.stories.quotesExtractor.Quote;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 22/12/2016.
 */

public class QuotesTable extends DatabaseTable {

    public static final String TABLE_NAME = "Quotes";
    public static final String TEXT = "Text";
    public static final String AUTHOR_ID = "AuthorId";
    public static final String QUOTE_ID = "QuoteId";

    @Override
    public String getCreateStatement() {
        addTableName(TABLE_NAME);
        addPrimaryKey(QUOTE_ID);
        addIntColumn(AUTHOR_ID);
        addString(TEXT, 256);

        return getResultQuery();
    }

    @Override
    public void delete() {
        QuotesApp.sQuotesSqlHelper.getWritableDatabase().delete(TABLE_NAME, null, null);
    }

    public void save(List<Quote> quotes, long authorId) {
        SQLiteDatabase database = QuotesApp.sQuotesSqlHelper.getWritableDatabase();

        database.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues();
            TagsTable tagsTable = new TagsTable();
            for (Quote quote : quotes) {
                contentValues.clear();
                contentValues.put(AUTHOR_ID, authorId);
                contentValues.put(TEXT, quote.getText());

                long id = database.insert(TABLE_NAME, null, contentValues);
                tagsTable.save(id, quote.getTags());
            }

            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }
    }

    public List<Quote> getQuoteByAuthor(long authorId) {
        Cursor cursor = QuotesApp.sQuotesSqlHelper.getReadableDatabase().query(
                TABLE_NAME,
                new String[]{QUOTE_ID, TEXT},
                AUTHOR_ID + " LIKE ?",
                new String[]{String.valueOf(authorId)},
                null,
                null,
                null);

        List<Quote> quotes = new ArrayList<>();
        TagsTable tagsTable = new TagsTable();
        if (cursor.moveToFirst()) {
            do {
                List<String> tags = tagsTable.getTags(cursor.getLong(0));
                String text = cursor.getString(1);

                quotes.add(new Quote(text, authorId, tags));
            } while (cursor.moveToNext());
        }
        cursor.close();

        return quotes;
    }
}
