package com.magycbytes.quotesapp.stories.quote.dataProvider.database;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;

import com.magycbytes.quotesapp.stories.quote.dataProvider.commonLogic.AuthorsQuotes;
import com.magycbytes.quotesapp.stories.quote.dataProvider.commonLogic.QuotesAsyncDataProvider;

/**
 * Created by Alex on 20/12/2016.
 */

public class QuotesDatabaseProvider extends QuotesAsyncDataProvider {

    private static final String LOG_TAG = QuotesDatabaseProvider.class.getSimpleName();

    private long mAuthorId;

    public QuotesDatabaseProvider(Context context, LoaderManager loaderManager, long authorId) {
        super(context, loaderManager, 1);
        mAuthorId = authorId;
    }

    @Override
    public Loader<AuthorsQuotes> onCreateLoader(int id, Bundle args) {
        Log.d(LOG_TAG, "Loading quotes from database");
        return new QuotesDatabaseAsyncLoader(mContext, mAuthorId);
    }
}
