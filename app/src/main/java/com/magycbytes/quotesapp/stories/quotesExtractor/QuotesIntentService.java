package com.magycbytes.quotesapp.stories.quotesExtractor;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.magycbytes.quotesapp.stories.newMainMenu.MainActivity;
import com.magycbytes.quotesapp.network.ApiManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.magycbytes.quotesapp.stories.newMainMenu.MainActivity.sQuoteList;


public class QuotesIntentService extends IntentService {

    private static final String LOG_TAG = QuotesIntentService.class.getSimpleName();

    public static final String AUTHOR_URL = "AuthorUrl";

    public static void start(Context context, String url) {
        Intent intent = new Intent(context, QuotesIntentService.class);
        intent.putExtra(AUTHOR_URL, url);
        context.startService(intent);
    }

    public QuotesIntentService() {
        super("QuotesIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {


        List<Quote> allExtractedQuotes = new ArrayList<>();
        try {
            String url = intent.getStringExtra(AUTHOR_URL);
            String authorUrl = url;

            Log.d(LOG_TAG, "Parsing url: " + url);

            QuotesParser parser = new QuotesParser();

            do {
                Response<ResponseBody> response = ApiManager.getApi().getQuotes(url).execute();
                InputStream inputStream = response.body().byteStream();
                List<Quote> quoteList = parser.parse(inputStream, authorUrl);
                inputStream.close();

                allExtractedQuotes.addAll(quoteList);
                url = parser.getLinkNextPage();
            } while (url != null);

        } catch (IOException e) {
            e.printStackTrace();
            new Gson().toJson(MainActivity.sQuoteList);
        } catch (Exception e) {
            int z = 10; z++;
            e.printStackTrace();
        }



        Log.d(LOG_TAG, "Existing quoutes " + sQuoteList.size() + ", adding new quotes " + allExtractedQuotes.size());
        sQuoteList.addAll(allExtractedQuotes);
    }
}
