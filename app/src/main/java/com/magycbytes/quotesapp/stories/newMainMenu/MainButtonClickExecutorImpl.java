package com.magycbytes.quotesapp.stories.newMainMenu;

import android.content.Context;
import com.magycbytes.quotesapp.stories.authors.AuthorsActivity;

/**
 * Created by alex on 20/07/2017.
 */
public class MainButtonClickExecutorImpl implements MainButtonClickExecutor {

    private Context mContext;

    public MainButtonClickExecutorImpl(Context context) {
        mContext = context;
    }

    @Override
    public void showAuthors() {
        AuthorsActivity.start(mContext);
    }
}
