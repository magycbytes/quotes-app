package com.magycbytes.quotesapp.stories.authors.dataProvider;

import com.magycbytes.quotesapp.stories.authorsExtractor.Author;

import java.util.List;

/**
 * Created by Alex on 20/12/2016.
 */

public interface AuthorDataProvider {

    void loadAllAuthors();

    void setEventsListener(Events listener);

    interface Events {
        void onAuthorsLoaded(List<Author> authors);
    }
}
