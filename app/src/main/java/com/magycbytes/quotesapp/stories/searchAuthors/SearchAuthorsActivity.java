package com.magycbytes.quotesapp.stories.searchAuthors;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.magycbytes.quotesapp.R;
import com.magycbytes.quotesapp.stories.authors.AuthorsFragment;

public class SearchAuthorsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_authors);

        // Get the intent, verify the action and get the query
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            executeSearch(query);
        }
    }

    private void executeSearch(String textToFind) {
        getSupportFragmentManager().beginTransaction().replace(R.id.activity_search_authors, AuthorsFragment.newInstance(textToFind)).commit();
    }
}
