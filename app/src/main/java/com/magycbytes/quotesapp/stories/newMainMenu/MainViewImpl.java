package com.magycbytes.quotesapp.stories.newMainMenu;

import android.view.View;
import com.magycbytes.quotesapp.R;
import com.magycbytes.quotesapp.stories.allTags.AllTagsActivity;

/**
 * Created by alex on 20/07/2017.
 */
public class MainViewImpl implements MainView, View.OnClickListener {

    private final QuoteView mQuoteView;
    private Events mEventsListener;

    MainViewImpl(final View view) {
        mQuoteView = new QuoteView(view);

        view.findViewById(R.id.authorsButton).setOnClickListener(this);
        view.findViewById(R.id.tagsButton).setOnClickListener(l -> AllTagsActivity.start(view.getContext()));
        view.findViewById(R.id.refreshQuote).setOnClickListener(l -> mEventsListener.onUpdateQuoteOfDay());
    }

    @Override
    public void showQuote(String quoteText, String quoteAuthor) {
        mQuoteView.show(quoteText, quoteAuthor);
    }

    @Override
    public void setEventsListener(Events eventsListener) {
        mEventsListener = eventsListener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.authorsButton:
                mEventsListener.onShowAuthors();
                break;
        }
    }
}
