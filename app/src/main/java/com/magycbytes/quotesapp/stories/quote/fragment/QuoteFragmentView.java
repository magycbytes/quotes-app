package com.magycbytes.quotesapp.stories.quote.fragment;

import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.magycbytes.quotesapp.R;
import com.magycbytes.quotesapp.stories.quotesByTag.QuotesByTagActivity;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.List;

/**
 * Created by Alex on 20/12/2016.
 */

public class QuoteFragmentView implements View.OnClickListener {

    private TextView mQuoteText;
    private TextView mTotalQuotes;
    private TextView mCurrentPosition;
    private LinearLayout mTagsContainer;

    QuoteFragmentView(View view) {
        mQuoteText = (TextView) view.findViewById(R.id.quote_text);
        mTotalQuotes = (TextView) view.findViewById(R.id.total_quotes);
        mCurrentPosition = (TextView) view.findViewById(R.id.current_quote_position);
        mTagsContainer = (LinearLayout) view.findViewById(R.id.tags_container);

        mQuoteText.setTypeface(Typeface.createFromAsset(mQuoteText.getContext().getAssets(), "helvetica.otf"));
    }

    void showQuote(String quoteText) {
        mQuoteText.setText(quoteText);
    }

    void showQuotePosition(int totalQuotes, int currentPosition) {
        mTotalQuotes.setText(String.valueOf(totalQuotes));
        mCurrentPosition.setText(String.valueOf(currentPosition + 1));
    }

    void showTags(List<String> tags) {
        mTagsContainer.removeAllViews();

        AttributeSet attributeSet = getAttributeSet(R.xml.view_tag);
        AttributeSet spaceAttributes = getAttributeSet(R.xml.view_space);

        for (String tag : tags) {
//            TextView child = new TextView(mTagsContainer.getContext(), attributeSet);
//            child.setText(tag);
//            mTagsContainer.addView(child);
//            Space space = new Space(mTagsContainer.getContext(), spaceAttributes);
//            mTagsContainer.addView(space);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(10, 0, 10, 0);

            TextView textView = (TextView) LayoutInflater.from(mTagsContainer.getContext()).inflate(R.layout.view_tag, null);
            textView.setText(tag);
            textView.setLayoutParams(params);
            textView.setOnClickListener(this);
            mTagsContainer.addView(textView);
        }
    }

    @Nullable
    private AttributeSet getAttributeSet(int resourceId) {
        XmlPullParser parser = mTagsContainer.getContext().getResources().getXml(resourceId);
        AttributeSet attributeSet = null;
        try {
            parser.next();
            parser.nextTag();
            attributeSet = Xml.asAttributeSet(parser);
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }
        return attributeSet;
    }


    @Override
    public void onClick(View view) {
        QuotesByTagActivity.start(view.getContext(), ((TextView) view).getText().toString());
    }
}
