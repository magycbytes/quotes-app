package com.magycbytes.quotesapp.stories.quotesByTag;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.magycbytes.quotesapp.R;

public class QuotesByTagActivity extends AppCompatActivity {

    public static final String TAG = "Tag";

    public static void start(Context context, String tag) {
        Intent intent = new Intent(context, QuotesByTagActivity.class);
        intent.putExtra(TAG, tag);

        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quotes_by_tag);

        String tag = getIntent().getStringExtra(TAG);
        setTitle("Quotes by tag - " + tag);

        QuoteByTagListView listView = new QuoteByTagListView(findViewById(R.id.activity_quotes_by_tag));
        QuotesByTagPresenter presenter = new QuotesByTagPresenter(new QuotesByTagJsonProvider(tag, this, getSupportLoaderManager()), listView);
        presenter.showQuotes();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
