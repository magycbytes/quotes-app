package com.magycbytes.quotesapp.stories.quote.dataProvider.commonLogic;

import android.content.Context;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.magycbytes.quotesapp.stories.quote.dataProvider.QuotesProvider;

/**
 * Created by Alex on 03/01/2017.
 */

public abstract class QuotesAsyncDataProvider implements QuotesProvider, LoaderManager.LoaderCallbacks<AuthorsQuotes> {

    protected Context mContext;
    private Events mListener;
    private LoaderManager mLoaderManager;
    private int mLoaderId;

    protected QuotesAsyncDataProvider(Context context, LoaderManager loaderManager, int loaderId) {
        mContext = context;
        mLoaderManager = loaderManager;
        mLoaderId = loaderId;
    }

    @Override
    public void getQuotes() {
        mLoaderManager.initLoader(mLoaderId, null, this);
    }

    @Override
    public void setListener(Events listener) {
        mListener = listener;
    }

    @Override
    public void onLoadFinished(Loader<AuthorsQuotes> loader, AuthorsQuotes data) {
        if (mListener != null) mListener.onQuotesReady(data);
    }

    @Override
    public void onLoaderReset(Loader<AuthorsQuotes> loader) {

    }
}
