package com.magycbytes.quotesapp.stories.quotesByTag;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.magycbytes.quotesapp.stories.authorsExtractor.Author;
import com.magycbytes.quotesapp.stories.quotesExtractor.Quote;
import com.magycbytes.quotesapp.util.JsonAuthors;
import com.magycbytes.quotesapp.util.JsonQuotes;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 03/01/2017.
 */

class QuotesByTagAsyncLoader extends AsyncTaskLoader<List<QuoteWithAuthor>> {

    private List<QuoteWithAuthor> mResponse;
    private String mTag;

    QuotesByTagAsyncLoader(Context context, String tag) {
        super(context);
        mTag = tag;
    }

    @Override
    public List<QuoteWithAuthor> loadInBackground() {
        List<Quote> tagQuotes = new ArrayList<>();

        JsonQuotes jsonQuotes = new JsonQuotes(getContext());
        for (int i = 0; i < jsonQuotes.getProvidersSize(); ++i) {
            List<Quote> quotes = jsonQuotes.deserializeQuotes(i);
            for (Quote quote : quotes) {
                if (quote.getTags().contains(mTag)) {
                    tagQuotes.add(quote);
                }
            }
        }

        List<QuoteWithAuthor> quoteWithAuthors = new ArrayList<>();

        List<Author> authors = JsonAuthors.getAuthors(getContext());
        for (Quote quote : tagQuotes) {
            for (Author author : authors) {
                if (author.getLink().equals(quote.getAuthorLink())) {
                    quoteWithAuthors.add(new QuoteWithAuthor(quote, author));
                    break;
                }
            }
        }

        return quoteWithAuthors;
    }

    @Override
    public void deliverResult(List<QuoteWithAuthor> data) {
        mResponse = data;

        if (isStarted()) super.deliverResult(data);
    }

    @Override
    protected void onStartLoading() {
        if (mResponse != null) deliverResult(mResponse);
        if (takeContentChanged() || mResponse == null) forceLoad();
    }
}
