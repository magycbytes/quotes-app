package com.magycbytes.quotesapp.stories.allTags;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.magycbytes.quotesapp.R;

import java.util.List;

/**
 * Created by Alex on 05/01/2017.
 */

public class AllTagsView {

    private RecyclerView mAllTagsContainer;

    AllTagsView(View view) {
        mAllTagsContainer = (RecyclerView) view.findViewById(R.id.tags_container);
    }

    void show(List<String> allTags) {
        mAllTagsContainer.setLayoutManager(new GridLayoutManager(mAllTagsContainer.getContext(), 3));
        mAllTagsContainer.setAdapter(new AllTagsAdapter(allTags));
    }
}
