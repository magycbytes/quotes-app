package com.magycbytes.quotesapp.stories.quote.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.quotesapp.R;
import com.magycbytes.quotesapp.util.ClipboardUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class QuoteFragment extends Fragment {


    public static final String QUOTE_TEXT = "QuoteText";
    public static final String QUESTIONS_COUNT = "QuestionsCount";
    public static final String CURRENT_QUESTION = "CurrentQuestion";
    public static final String TAGS = "Tags";

    public QuoteFragment() {
        // Required empty public constructor
    }

    public static Fragment newInstance(String text, int questionsCount, int currentQuestion, List<String> tags) {
        Bundle arguments = new Bundle();
        arguments.putString(QUOTE_TEXT, text);
        arguments.putInt(QUESTIONS_COUNT, questionsCount);
        arguments.putInt(CURRENT_QUESTION, currentQuestion);
        arguments.putStringArrayList(TAGS, (ArrayList<String>) tags);

        QuoteFragment quoteFragment = new QuoteFragment();
        quoteFragment.setArguments(arguments);

        return quoteFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_quote, container, false);

        QuoteFragmentView quoteFragmentView = new QuoteFragmentView(view);

        Bundle arguments = getArguments();
        if (arguments != null) {
            quoteFragmentView.showQuote(arguments.getString(QUOTE_TEXT));
            quoteFragmentView.showQuotePosition(arguments.getInt(QUESTIONS_COUNT), arguments.getInt(CURRENT_QUESTION));
            quoteFragmentView.showTags(arguments.getStringArrayList(TAGS));
        }

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_quote_fragment, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.copy_quote:
                String string = getArguments().getString(QUOTE_TEXT);
                ClipboardUtil.copyText(getContext(), string);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
