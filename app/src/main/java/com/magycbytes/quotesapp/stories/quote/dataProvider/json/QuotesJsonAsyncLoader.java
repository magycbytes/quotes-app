package com.magycbytes.quotesapp.stories.quote.dataProvider.json;

import android.content.Context;

import com.magycbytes.quotesapp.stories.quote.QuotesViewProgress;
import com.magycbytes.quotesapp.stories.quote.dataProvider.commonLogic.AuthorsQuotes;
import com.magycbytes.quotesapp.stories.quote.dataProvider.commonLogic.QuotesAsyncTaskLoader;
import com.magycbytes.quotesapp.stories.quotesExtractor.Quote;
import com.magycbytes.quotesapp.util.JsonQuotes;

import java.util.List;

/**
 * Created by Alex on 22/12/2016.
 */

class QuotesJsonAsyncLoader extends QuotesAsyncTaskLoader {

    private String mAuthorLink;

    QuotesJsonAsyncLoader(Context context, String authorLink) {
        super(context);
        mAuthorLink = authorLink;
    }

    @Override
    public AuthorsQuotes loadInBackground() {
        JsonQuotes jsonQuotes = new JsonQuotes(getContext());
        List<Quote> authorsQuotes = jsonQuotes.getAuthorsQuotes(mAuthorLink);

        QuotesViewProgress progress = new QuotesViewProgress(getContext());

        return new AuthorsQuotes(authorsQuotes, progress.getProgress(mAuthorLink));
    }
}
