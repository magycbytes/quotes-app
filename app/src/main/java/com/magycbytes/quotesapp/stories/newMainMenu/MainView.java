package com.magycbytes.quotesapp.stories.newMainMenu;

/**
 * Created by alex on 20/07/2017.
 */
public interface MainView {

    void showQuote(String quoteText, String quoteAuthor);

    void setEventsListener(Events eventsListener);

    interface Events {
        void onShowAuthors();

        void onUpdateQuoteOfDay();
    }
}
