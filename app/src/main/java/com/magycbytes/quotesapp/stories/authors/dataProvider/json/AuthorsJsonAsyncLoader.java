package com.magycbytes.quotesapp.stories.authors.dataProvider.json;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.magycbytes.quotesapp.R;
import com.magycbytes.quotesapp.stories.authorsExtractor.Author;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 03/01/2017.
 */

class AuthorsJsonAsyncLoader extends AsyncTaskLoader<List<Author>> {

    private List<Author> mResponse;
    private String mFilter;

    AuthorsJsonAsyncLoader(Context context, String filter) {
        super(context);
        mFilter = filter;
    }

    @Override
    public List<Author> loadInBackground() {
        InputStream raw = getContext().getResources().openRawResource(R.raw.authors);
        Reader rd = new BufferedReader(new InputStreamReader(raw));

        List<Author> authors = new Gson().fromJson(rd, new TypeToken<ArrayList<Author>>() {
        }.getType());
        if (mFilter != null) {
            List<Author> filteredAuthors = new ArrayList<>();
            String lowerCaseFilter = mFilter.toLowerCase();
            for (Author author : authors) {
                if (author.getName().toLowerCase().contains(lowerCaseFilter)) filteredAuthors.add(author);
            }
            return filteredAuthors;
        }

        return authors;
    }

    @Override
    public void deliverResult(List<Author> data) {
        mResponse = data;

        if (isStarted()) super.deliverResult(data);
    }

    @Override
    protected void onStartLoading() {
        if (mResponse != null) deliverResult(mResponse);
        if (takeContentChanged() || mResponse == null) forceLoad();
    }
}
