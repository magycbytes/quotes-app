package com.magycbytes.quotesapp.stories.quote.dataProvider.database;

import android.content.Context;

import com.magycbytes.quotesapp.database.QuotesTable;
import com.magycbytes.quotesapp.stories.quote.dataProvider.commonLogic.AuthorsQuotes;
import com.magycbytes.quotesapp.stories.quote.dataProvider.commonLogic.QuotesAsyncTaskLoader;
import com.magycbytes.quotesapp.stories.quotesExtractor.Quote;

import java.util.List;

/**
 * Created by Alex on 22/12/2016.
 */

class QuotesDatabaseAsyncLoader extends QuotesAsyncTaskLoader {

    private long mAuthorId;

    QuotesDatabaseAsyncLoader(Context context, long authorId) {
        super(context);
        mAuthorId = authorId;
    }

    @Override
    public AuthorsQuotes loadInBackground() {
        QuotesTable quotesTable = new QuotesTable();
        List<Quote> quoteByAuthor = quotesTable.getQuoteByAuthor(mAuthorId);
        return new AuthorsQuotes(quoteByAuthor, 0);
    }
}
