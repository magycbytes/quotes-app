package com.magycbytes.quotesapp.stories.authorsExtractor;

/**
 * Created by Alex on 19/12/2016.
 */

public class Author {

    private String mName;
    private String mLink;
    private long mId;

    public Author(String name, long id) {
        mName = name;
        mId = id;
    }

    public Author(String name, String link) {
        mName = name;
        mLink = link;
    }

    public String getName() {
        return mName;
    }

    public String getLink() {
        return mLink;
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }
}
