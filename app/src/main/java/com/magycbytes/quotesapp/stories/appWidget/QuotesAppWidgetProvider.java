package com.magycbytes.quotesapp.stories.appWidget;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;
import com.magycbytes.quotesapp.R;
import com.magycbytes.quotesapp.stories.newMainMenu.QuoteViewModel;
import com.magycbytes.quotesapp.stories.quoteOfDay.SingleQuoteExtractor;

/**
 * Created by alex on 10/07/2017.
 */
public class QuotesAppWidgetProvider extends AppWidgetProvider {

    private static final String LOG_TAG = QuotesAppWidgetProvider.class.getSimpleName();

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int ignored : appWidgetIds) {
            Log.d(LOG_TAG, "Updating the widget");
            context.startService(new Intent(context, UpdateQuoteService.class));
        }
    }

    public static class UpdateQuoteService extends Service {

        @Override
        public int onStartCommand(Intent intent, int flags, int startId) {
            RemoteViews remoteViews = buildUpdate(this);
            AppWidgetManager manager = AppWidgetManager.getInstance(this);
            manager.updateAppWidget(new ComponentName(this, QuotesAppWidgetProvider.class), remoteViews);
            Log.d(LOG_TAG, "Showing the quote");

            return 0;
        }

        RemoteViews buildUpdate(Context context) {
            QuoteViewModel quoteToShow = SingleQuoteExtractor.extractRandomQuestion(context);

            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.quotes_app_widget);
            showQuoteInWidget(quoteToShow, remoteViews);
            appendListenerForChangeQuote(context, remoteViews);

            return remoteViews;
        }

        private void showQuoteInWidget(QuoteViewModel quoteToShow, RemoteViews remoteViews) {
            remoteViews.setTextViewText(R.id.quoteText, quoteToShow.getQuoteText());
            remoteViews.setTextViewText(R.id.authorName, quoteToShow.getQuoteAuthor());
        }

        private void appendListenerForChangeQuote(Context context, RemoteViews remoteViews) {
            Intent intent = new Intent(context, UpdateQuoteService.class);
            PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, 0);
            remoteViews.setOnClickPendingIntent(R.id.rootLayout, pendingIntent);
        }


        @Override
        public IBinder onBind(Intent intent) {
            return null;
        }
    }
}
