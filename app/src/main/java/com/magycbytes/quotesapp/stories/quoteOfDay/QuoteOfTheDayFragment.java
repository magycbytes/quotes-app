package com.magycbytes.quotesapp.stories.quoteOfDay;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.quotesapp.R;

public class QuoteOfTheDayFragment extends Fragment {


    public static QuoteOfTheDayFragment newInstance() {
        return new QuoteOfTheDayFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_quote_of_the_day, container, false);

        QuoteOfDayView quoteOfDayView = new QuoteOfDayView(view);
        QuoteOfDayPresenter presenter = new QuoteOfDayPresenter(new QuoteOfDayDatabaseProvider(getContext()), quoteOfDayView);
        presenter.showTheQuote();

        return view;
    }

}
