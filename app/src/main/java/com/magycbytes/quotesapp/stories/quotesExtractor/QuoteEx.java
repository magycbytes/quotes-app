package com.magycbytes.quotesapp.stories.quotesExtractor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 20/12/2016.
 */

public class QuoteEx {

    private String mText;
    private String mLink;
    private List<String> mTags = new ArrayList<>();
    private int mId;
    private int mAuthorId;

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    public String getLink() {
        return mLink;
    }

    public void setLink(String link) {
        mLink = link;
    }

    public List<String> getTags() {
        return mTags;
    }

    public void setTags(List<String> tags) {
        mTags = tags;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getAuthorId() {
        return mAuthorId;
    }

    public void setAuthorId(int authorId) {
        mAuthorId = authorId;
    }
}
