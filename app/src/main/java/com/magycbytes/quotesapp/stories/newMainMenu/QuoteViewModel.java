package com.magycbytes.quotesapp.stories.newMainMenu;

/**
 * Created by alex on 20/07/2017.
 */
public class QuoteViewModel {

    private String mQuoteText;
    private String mQuoteAuthor;

    public QuoteViewModel(String quoteText, String quoteAuthor) {
        mQuoteText = quoteText;
        mQuoteAuthor = quoteAuthor;
    }

    public String getQuoteText() {
        return mQuoteText;
    }

    public String getQuoteAuthor() {
        return mQuoteAuthor;
    }
}
