package com.magycbytes.quotesapp.stories.authors.dataProvider.database;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.magycbytes.quotesapp.stories.authors.dataProvider.commonLogic.AuthorsAsyncDataProvider;
import com.magycbytes.quotesapp.stories.authorsExtractor.Author;

import java.util.List;

/**
 * Created by Alex on 03/01/2017.
 */

public class AuthorsDatabaseProvider extends AuthorsAsyncDataProvider {

    public AuthorsDatabaseProvider(LoaderManager loaderManager, String filter, Context context) {
        super(loaderManager, filter, context, 1);
    }

    @Override
    public Loader<List<Author>> onCreateLoader(int id, Bundle args) {
        return new AuthorsDatabaseAsyncLoader(mContext, mFilter);
    }
}
