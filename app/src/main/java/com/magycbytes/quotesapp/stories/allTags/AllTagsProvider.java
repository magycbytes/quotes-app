package com.magycbytes.quotesapp.stories.allTags;

import java.util.List;

/**
 * Created by Alex on 09/01/2017.
 */

public interface AllTagsProvider {

    void getAllTags();

    void setListener(Events listener);

    interface Events {
        void onTagsAvailable(List<String> tags);
    }
}
