package com.magycbytes.quotesapp.stories.quote.dataProvider.commonLogic;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

/**
 * Created by Alex on 03/01/2017.
 */

public abstract class QuotesAsyncTaskLoader extends AsyncTaskLoader<AuthorsQuotes> {

    private AuthorsQuotes mQuotes;

    public QuotesAsyncTaskLoader(Context context) {
        super(context);
    }

    @Override
    public void deliverResult(AuthorsQuotes data) {
        mQuotes = data;

        if (isStarted()) super.deliverResult(data);
    }

    @Override
    protected void onStartLoading() {
        if (mQuotes != null) deliverResult(mQuotes);
        if (takeContentChanged() || mQuotes == null) forceLoad();
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
        mQuotes = null;
    }
}
