package com.magycbytes.quotesapp.stories.quoteOfDay;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.magycbytes.quotesapp.R;
import com.magycbytes.quotesapp.stories.authorsExtractor.Author;
import com.magycbytes.quotesapp.stories.quotesExtractor.Quote;
import com.magycbytes.quotesapp.util.JsonQuotes;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * Created by Alex on 21/12/2016.
 */

public class QuoteOfDayDatabaseProvider implements QuoteOfDayProvider {

    private static final String FILE_INDEX = "FileIndex";
    private static final String QUOTE_INDEX = "QuoteIndex";
    private static final String LAST_UPDATED = "LastUpdated";
    private QuoteOfDayProvider.Events mListener;
    private Context mContext;
    private JsonQuotes mJsonQuotes;

    QuoteOfDayDatabaseProvider(Context context) {
        mContext = context;

        mJsonQuotes = new JsonQuotes(context);
    }

    @Override
    public void getTheQuote() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);

        int fileIndex;
        int quoteIndex = -1;

        if (!isNeededAnotherQuote()) {
            fileIndex = preferences.getInt(FILE_INDEX, 0);
            quoteIndex = preferences.getInt(QUOTE_INDEX, 0);

            provideQuote(fileIndex, quoteIndex);
        } else {
            fileIndex = getFileIndex();
            quoteIndex = provideQuote(fileIndex, quoteIndex);

            preferences.edit().putInt(FILE_INDEX, fileIndex).apply();
            preferences.edit().putInt(QUOTE_INDEX, quoteIndex).apply();
            preferences.edit().putLong(LAST_UPDATED, System.currentTimeMillis()).apply();
        }
    }

    private int getFileIndex() {
        int fileIndex;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fileIndex = ThreadLocalRandom.current().nextInt(0, mJsonQuotes.getProvidersSize());
        } else {
            Random random = new Random();
            fileIndex = random.nextInt(mJsonQuotes.getProvidersSize());
        }
        return fileIndex;
    }

    @Override
    public void setEventsListener(Events eventsListener) {
        mListener = eventsListener;
    }


    public static Author getAuthor(Context context, String authorLink) {
        InputStream raw = context.getResources().openRawResource(R.raw.authors);
        Reader rd = new BufferedReader(new InputStreamReader(raw));
        List<Author> authors = new Gson().fromJson(rd, new TypeToken<ArrayList<Author>>() {
        }.getType());

        Author resultAuthor = new Author("", -1);

        for (Author author : authors) {
            if (author.getLink().equals(authorLink)) {
                resultAuthor = author;
            }
        }

        return resultAuthor;
    }

    private int provideQuote(int fileIndex, int quoteIndex) {
        List<Quote> quotes = mJsonQuotes.deserializeQuotes(fileIndex);

        if (quoteIndex < 0) {
            Random random = new Random();
            quoteIndex = random.nextInt(quotes.size());
        }

        Author author = getAuthor(mContext, quotes.get(quoteIndex).getAuthorLink());

        if (mListener != null) mListener.onQuoteOfDayAvailable(quotes.get(quoteIndex), author);

        return quoteIndex;
    }

    private boolean isNeededAnotherQuote() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);

        long lastUpdated = preferences.getLong(LAST_UPDATED, 0);
        Date lastUpdatedDate = new Date(lastUpdated);
        Date todayDate = Calendar.getInstance().getTime();
        lastUpdatedDate.setHours(0);
        lastUpdatedDate.setMinutes(0);
        lastUpdatedDate.setSeconds(0);
        todayDate.setMinutes(0);
        todayDate.setSeconds(0);
        todayDate.setHours(0);

        return todayDate.getTime() - lastUpdatedDate.getTime() < TimeUnit.SECONDS.toMillis(1);
    }
}
