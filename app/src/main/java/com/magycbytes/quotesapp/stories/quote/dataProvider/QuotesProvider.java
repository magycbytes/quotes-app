package com.magycbytes.quotesapp.stories.quote.dataProvider;

import com.magycbytes.quotesapp.stories.quote.dataProvider.commonLogic.AuthorsQuotes;

/**
 * Created by Alex on 20/12/2016.
 */

public interface QuotesProvider {

    void getQuotes();

    void setListener(Events listener);

    interface Events {
        void onQuotesReady(AuthorsQuotes quotes);
    }
}
