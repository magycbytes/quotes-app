package com.magycbytes.quotesapp.stories.allTags;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.quotesapp.R;

import java.util.List;

/**
 * Created by Alex on 05/01/2017.
 */

public class AllTagsAdapter extends RecyclerView.Adapter<TagViewHolder> {

    private List<String> mTags;

    public AllTagsAdapter(List<String> tags) {
        mTags = tags;
    }

    @Override
    public TagViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_tag, parent, false);
        return new TagViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TagViewHolder holder, int position) {
        holder.show(mTags.get(position));
    }

    @Override
    public int getItemCount() {
        return mTags.size();
    }
}
