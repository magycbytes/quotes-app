package com.magycbytes.quotesapp.stories.intro;


import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.magycbytes.quotesapp.database.AuthorsTable;
import com.magycbytes.quotesapp.database.QuotesTable;
import com.magycbytes.quotesapp.database.TagsTable;
import com.magycbytes.quotesapp.stories.authorsExtractor.Author;
import com.magycbytes.quotesapp.stories.quotesExtractor.Quote;
import com.magycbytes.quotesapp.util.ApplicationSettings;
import com.magycbytes.quotesapp.util.JsonAuthors;
import com.magycbytes.quotesapp.util.JsonQuotes;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Alex on 22/12/2016.
 */

public class DeserializatorLoader extends AsyncTaskLoader<Boolean> {

    private static final String LOG_TAG = DeserializatorLoader.class.getSimpleName();
    private Boolean mResponse = null;

    public DeserializatorLoader(Context context) {
        super(context);
    }

    @Override
    public Boolean loadInBackground() {
        Log.d(LOG_TAG, "Extracting authors from JSON");
        List<Author> authors = JsonAuthors.getAuthors(getContext());


        JsonQuotes jsonQuotes = new JsonQuotes(getContext());
        AuthorsTable authorsTable = new AuthorsTable();
        QuotesTable quotesTable = new QuotesTable();
        TagsTable tagsTable = new TagsTable();

        authorsTable.delete();
        quotesTable.delete();
        tagsTable.delete();

        Log.d(LOG_TAG, "Extracting all quotes");
        List<Quote> allQuotes = jsonQuotes.getAllQuotes();

        long currentTIme = System.currentTimeMillis();
        int procents = 0;
        for (Author author : authors) {
            procents++;
            long authorId = authorsTable.saveAuthor(author.getName());
            Log.d(LOG_TAG, "Author " + author.getName() + " was saved with id " + authorId + ". " + procents + " - " + authors.size());

            Log.d(LOG_TAG, "Find authors quotes");
            List<Quote> authorsQuotes = findAuthorQuotes(allQuotes, author.getLink());

            Log.d(LOG_TAG, "Saving authors quotes......");
            quotesTable.save(authorsQuotes, authorId);

            Log.d(LOG_TAG, "Finished saving.\n");
        }

        long differenceInTime = System.currentTimeMillis() - currentTIme;
        Log.d(LOG_TAG, "The whole process was taken " + TimeUnit.MILLISECONDS.toSeconds(differenceInTime));

        ApplicationSettings applicationSettings = new ApplicationSettings(getContext());
        applicationSettings.dataWasDeserialized();

        return true;
    }

    @Override
    public void deliverResult(Boolean data) {
        mResponse = data;

        if (isStarted()) super.deliverResult(data);
    }

    @Override
    protected void onStartLoading() {
        if (mResponse != null) deliverResult(mResponse);
        if (takeContentChanged() || mResponse == null) forceLoad();
    }

    private List<Quote> findAuthorQuotes(List<Quote> quotes, String authorLink) {

        List<Quote> authorsQuotes = new ArrayList<>();
        for (Quote quote : quotes) {
            if (quote.getAuthorLink().equals(authorLink)) {
                authorsQuotes.add(quote);
            } else {
                if (!authorsQuotes.isEmpty()) break;
            }
        }

        return authorsQuotes;
    }
}
