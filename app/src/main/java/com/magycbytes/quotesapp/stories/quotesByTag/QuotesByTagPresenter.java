package com.magycbytes.quotesapp.stories.quotesByTag;

import java.util.List;

/**
 * Created by Alex on 21/12/2016.
 */

class QuotesByTagPresenter implements QuotesByTagProvider.Events {

    private QuotesByTagProvider mProvider;
    private QuoteByTagListView mListView;

    QuotesByTagPresenter(QuotesByTagProvider provider, QuoteByTagListView listView) {
        mProvider = provider;
        mListView = listView;

        mProvider.setEventsListener(this);
    }

    void showQuotes() {
        mListView.showLoadProgress();
        mProvider.getQuotes();
    }

    @Override
    public void onQuotesLoaded(List<QuoteWithAuthor> quotes) {
        mListView.show(quotes);
        mListView.hideLoadProgress();
    }
}
