package com.magycbytes.quotesapp.stories.quotesExtractor;

import android.util.Log;

import com.magycbytes.quotesapp.network.ApiManager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 20/12/2016.
 */

public class QuotesParser {

    private static final String LOG_TAG = QuotesParser.class.getSimpleName();
    private String mLinkNextPage;

    public List<Quote> parse(InputStream inputStream, String authorLink) throws IOException {
        Document document = Jsoup.parse(inputStream, null, ApiManager.BASE_URL);
        Elements qoutesBox = document.getElementsByClass("masonryitem");

        Elements pageContainer = document.getElementsByClass("paginationContainer");
        if (pageContainer.isEmpty()) {
            mLinkNextPage = null;
        } else {
            Elements pagesNumbers = pageContainer.get(0).getElementsByTag("li");
            if (pagesNumbers == null || pagesNumbers.isEmpty()) {
                mLinkNextPage = null;
            } else {
                String pageClass = pagesNumbers.last().attr("class");
                if (pageClass != null && pageClass.equals("disabled")) {
                    mLinkNextPage = null;
                } else {
                    mLinkNextPage = pagesNumbers.last().getElementsByTag("a").get(0).attr("href");
                }
            }
        }

        if (qoutesBox == null || qoutesBox.isEmpty()) {
            Log.w(LOG_TAG, "No quotes found");
            return null;
        }

        List<Quote> extractedQuotes = new ArrayList<>();
        for (Element qouteBox : qoutesBox) {
            Quote quote = new Quote();

            Elements tagsBox = qouteBox.getElementsByClass("bq_boxyRelatedLeft");
            if (!tagsBox.isEmpty()) {
                Elements tags = tagsBox.get(0).getElementsByTag("a");
                for (Element tag : tags) {
                    quote.addTag(tag.text());
                }
            }

            Elements boxLink = qouteBox.getElementsByClass("bqQuoteLink");

            quote.setText(boxLink.text());
            quote.setLink(boxLink.get(0).getElementsByTag("a").get(0).attr("href"));
            quote.setAuthorLink(authorLink);

            extractedQuotes.add(quote);
        }

        return extractedQuotes;
    }

    public String getLinkNextPage() {
        return mLinkNextPage;
    }
}
