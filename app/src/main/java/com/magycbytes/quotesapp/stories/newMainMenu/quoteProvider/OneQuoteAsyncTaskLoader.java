package com.magycbytes.quotesapp.stories.newMainMenu.quoteProvider;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import com.magycbytes.quotesapp.stories.newMainMenu.QuoteViewModel;
import com.magycbytes.quotesapp.stories.quote.QuotesViewProgress;
import com.magycbytes.quotesapp.stories.quote.dataProvider.commonLogic.AuthorsQuotes;
import com.magycbytes.quotesapp.stories.quoteOfDay.SingleQuoteExtractor;
import com.magycbytes.quotesapp.stories.quotesExtractor.Quote;
import com.magycbytes.quotesapp.util.JsonQuotes;

import java.util.List;

/**
 * Created by Alex on 03/01/2017.
 */

public class OneQuoteAsyncTaskLoader extends AsyncTaskLoader<QuoteViewModel> {

    private QuoteViewModel mQuote;

    public OneQuoteAsyncTaskLoader(Context context) {
        super(context);
    }

    @Override
    public QuoteViewModel loadInBackground() {
        return SingleQuoteExtractor.extractRandomQuestion(getContext());
    }

    @Override
    public void deliverResult(QuoteViewModel data) {
        mQuote = data;

        if (isStarted()) super.deliverResult(data);
    }

    @Override
    protected void onStartLoading() {
        if (mQuote != null) deliverResult(mQuote);
        if (takeContentChanged() || mQuote == null) forceLoad();
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
        mQuote = null;
    }
}
