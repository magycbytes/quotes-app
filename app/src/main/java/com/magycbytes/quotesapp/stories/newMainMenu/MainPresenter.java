package com.magycbytes.quotesapp.stories.newMainMenu;

/**
 * Created by alex on 20/07/2017.
 */
public class MainPresenter implements QuoteOfDayProvider.Events, MainView.Events {

    private MainView mMainView;
    private QuoteOfDayProvider mDataProvider;
    private MainButtonClickExecutor mClickExecutor;

    MainPresenter(MainView mainView, QuoteOfDayProvider dataProvider, MainButtonClickExecutor clickExecutor) {
        mMainView = mainView;
        mDataProvider = dataProvider;
        mClickExecutor = clickExecutor;

        mainView.setEventsListener(this);
        mDataProvider.setEventsListener(this);
    }

    void showQuoteOfDay() {
        mDataProvider.loadQuoteOfDay();
    }

    @Override
    public void onQuoteOfDayAvailable(QuoteViewModel quoteViewModel) {
        mMainView.showQuote(quoteViewModel.getQuoteText(), quoteViewModel.getQuoteAuthor());
    }

    @Override
    public void onShowAuthors() {
        mClickExecutor.showAuthors();
    }

    @Override
    public void onUpdateQuoteOfDay() {
        mDataProvider.loadAnotherQuote();
    }
}
