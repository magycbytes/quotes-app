package com.magycbytes.quotesapp.stories.quotesByTag;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.quotesapp.R;

import java.util.List;

/**
 * Created by Alex on 21/12/2016.
 */

class QuoteListAdapter extends RecyclerView.Adapter<QuoteViewHolder> {


    private List<QuoteWithAuthor> mQuoteWithAuthors;

    QuoteListAdapter(List<QuoteWithAuthor> quoteWithAuthors) {
        mQuoteWithAuthors = quoteWithAuthors;
    }

    @Override
    public QuoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_quote, parent, false);
        return new QuoteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(QuoteViewHolder holder, int position) {
        holder.show(mQuoteWithAuthors.get(position).getAuthor(), mQuoteWithAuthors.get(position).getQuote());
    }

    @Override
    public int getItemCount() {
        return mQuoteWithAuthors.size();
    }
}
