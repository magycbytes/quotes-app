package com.magycbytes.quotesapp.stories.authors;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.magycbytes.quotesapp.R;

public class AuthorsActivity extends AppCompatActivity {

    public static void start(Context context) {
        context.startActivity(new Intent(context, AuthorsActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authors);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, AuthorsFragment.newInstance())
                .commit();
    }
}
