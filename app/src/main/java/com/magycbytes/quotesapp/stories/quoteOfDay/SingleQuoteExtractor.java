package com.magycbytes.quotesapp.stories.quoteOfDay;

import android.content.Context;
import com.magycbytes.quotesapp.stories.newMainMenu.QuoteViewModel;
import com.magycbytes.quotesapp.stories.quotesExtractor.Quote;
import com.magycbytes.quotesapp.util.JsonQuotes;

import java.util.Collections;
import java.util.List;

/**
 * Created by alex on 20/07/2017.
 */
public class SingleQuoteExtractor {

    public static QuoteViewModel extractRandomQuestion(Context context) {
        JsonQuotes jsonQuotes = new JsonQuotes(context);
        List<Quote> allQuotes = jsonQuotes.getAllQuotes();
        Collections.shuffle(allQuotes);

        Quote quoteToShow = allQuotes.get(0);
        String authorName = QuoteOfDayDatabaseProvider.getAuthor(context, quoteToShow.getAuthorLink()).getName();
        String quoteText = quoteToShow.getText();

        return new QuoteViewModel(quoteText, authorName);
    }
}
