package com.magycbytes.quotesapp.stories.authors;

import com.magycbytes.quotesapp.stories.authors.dataProvider.AuthorDataProvider;
import com.magycbytes.quotesapp.stories.authorsExtractor.Author;

import java.util.List;

/**
 * Created by Alex on 20/12/2016.
 */

class AuthorsListPresenter implements AuthorDataProvider.Events {

    private AuthorDataProvider mDataProvider;
    private AuthorsListView mView;

    AuthorsListPresenter(AuthorDataProvider dataProvider, AuthorsListView view) {
        mDataProvider = dataProvider;
        mView = view;
        mDataProvider.setEventsListener(this);
    }

    void load() {
        mDataProvider.loadAllAuthors();
    }

    @Override
    public void onAuthorsLoaded(List<Author> authors) {
        mView.showAuthors(authors);
    }
}
