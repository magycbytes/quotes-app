package com.magycbytes.quotesapp.stories.quotesExtractor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 20/12/2016.
 */

public class AuthorEx {

    private String mName;
    private List<QuoteEx> mQuoteList = new ArrayList<>();
    private int mId;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public void addQuote(QuoteEx quoteEx) {
        mQuoteList.add(quoteEx);
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }
}
