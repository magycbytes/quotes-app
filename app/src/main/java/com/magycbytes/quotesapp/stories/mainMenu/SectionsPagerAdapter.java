package com.magycbytes.quotesapp.stories.mainMenu;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.magycbytes.quotesapp.stories.allTags.AllTagsFragment;
import com.magycbytes.quotesapp.stories.authors.AuthorsFragment;
import com.magycbytes.quotesapp.stories.quoteOfDay.QuoteOfTheDayFragment;

import java.util.ArrayList;
import java.util.List;


class SectionsPagerAdapter extends FragmentPagerAdapter {

    private List<String> mSectionsTitles = new ArrayList<>();

    SectionsPagerAdapter(FragmentManager fm) {
        super(fm);

        // TODO move to strings for translations
        mSectionsTitles.add("Authors");
        mSectionsTitles.add("Quote of the day");
        mSectionsTitles.add("Tags");
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return AuthorsFragment.newInstance();
            case 1:
                return QuoteOfTheDayFragment.newInstance();
            case 2:
                return AllTagsFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mSectionsTitles.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mSectionsTitles.get(position);
    }
}
