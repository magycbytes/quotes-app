package com.magycbytes.quotesapp.stories.quote.dataProvider.commonLogic;

import com.magycbytes.quotesapp.stories.quotesExtractor.Quote;

import java.util.List;

/**
 * Created by Alex on 06/01/2017.
 */

public class AuthorsQuotes {

    private List<Quote> mQuotes;
    private int mLastPosition;

    public AuthorsQuotes(List<Quote> quotes, int lastPosition) {
        mQuotes = quotes;
        mLastPosition = lastPosition;
    }

    public List<Quote> getQuotes() {
        return mQuotes;
    }

    public void setQuotes(List<Quote> quotes) {
        mQuotes = quotes;
    }

    public int getLastPosition() {
        return mLastPosition;
    }

    public void setLastPosition(int lastPosition) {
        mLastPosition = lastPosition;
    }
}
