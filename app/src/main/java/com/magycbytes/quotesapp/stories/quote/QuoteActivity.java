package com.magycbytes.quotesapp.stories.quote;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.magycbytes.quotesapp.R;
import com.magycbytes.quotesapp.stories.authorsExtractor.Author;
import com.magycbytes.quotesapp.stories.quote.dataProvider.database.QuotesDatabaseProvider;
import com.magycbytes.quotesapp.stories.quote.dataProvider.json.QuotesJsonProvider;
import com.magycbytes.quotesapp.stories.quote.dataProvider.QuotesProvider;
import com.magycbytes.quotesapp.util.ApplicationSettings;

public class QuoteActivity extends AppCompatActivity {

    private static final String AUTHOR_LINK = "AuthorLink";
    public static final String AUTHOR_NAME = "AuthorName";
    public static final String AUTHOR_ID = "AuthorId";
    private QuotePresenter mPresenter;

    public static void start(Context context, Author author) {
        Intent intent = new Intent(context, QuoteActivity.class);
        if (author.getLink() == null) {
            intent.putExtra(AUTHOR_ID, author.getId());
        } else {
            intent.putExtra(AUTHOR_LINK, author.getLink());
        }
        intent.putExtra(AUTHOR_NAME, author.getName());
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quote);

        setTitle(getIntent().getStringExtra(AUTHOR_NAME));

        QuoteView quoteView = new QuoteView(findViewById(R.id.activity_quote), getSupportFragmentManager());

        mPresenter = new QuotePresenter(getQuotesProvider(), quoteView);
        mPresenter.load();
    }

    @NonNull
    private QuotesProvider getQuotesProvider() {
        ApplicationSettings applicationSettings = new ApplicationSettings(this);
        QuotesProvider quotesProvider;
        if (applicationSettings.isDataDeserialized()) {
            quotesProvider = new QuotesDatabaseProvider(this, getSupportLoaderManager(), getIntent().getLongExtra(AUTHOR_ID, -1));
        } else {
            quotesProvider = new QuotesJsonProvider(this, getIntent().getStringExtra(AUTHOR_LINK), getSupportLoaderManager());
        }
        return new QuotesJsonProvider(this, getIntent().getStringExtra(AUTHOR_LINK), getSupportLoaderManager());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        mPresenter.saveViewProgress(getIntent().getStringExtra(AUTHOR_LINK), this);
        super.onStop();
    }
}
