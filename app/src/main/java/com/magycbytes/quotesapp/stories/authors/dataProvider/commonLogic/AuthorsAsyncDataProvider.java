package com.magycbytes.quotesapp.stories.authors.dataProvider.commonLogic;

import android.content.Context;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.magycbytes.quotesapp.stories.authors.dataProvider.AuthorDataProvider;
import com.magycbytes.quotesapp.stories.authorsExtractor.Author;

import java.util.List;

/**
 * Created by Alex on 03/01/2017.
 */

public abstract class AuthorsAsyncDataProvider implements AuthorDataProvider, LoaderManager.LoaderCallbacks<List<Author>> {

    private Events mListener;
    private LoaderManager mLoaderManager;
    protected String mFilter;
    protected Context mContext;
    private int mLoaderId;

    protected AuthorsAsyncDataProvider(LoaderManager loaderManager, String filter, Context context, int loaderId) {
        mLoaderManager = loaderManager;
        mFilter = filter;
        mContext = context;
        mLoaderId = loaderId;
    }

    @Override
    public void setEventsListener(Events listener) {
        mListener = listener;
    }

    @Override
    public void loadAllAuthors() {
        mLoaderManager.initLoader(1, null, this);
    }

    @Override
    public void onLoadFinished(Loader<List<Author>> loader, List<Author> data) {
        mListener.onAuthorsLoaded(data);
    }

    @Override
    public void onLoaderReset(Loader<List<Author>> loader) {

    }
}
