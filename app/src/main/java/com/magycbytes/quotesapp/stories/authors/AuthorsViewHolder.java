package com.magycbytes.quotesapp.stories.authors;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.magycbytes.quotesapp.R;

/**
 * Created by Alex on 20/12/2016.
 */

class AuthorsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView mAuthorName;
    private AuthorsViewHolderEvents mListener;

    AuthorsViewHolder(View itemView, AuthorsViewHolderEvents listener) {
        super(itemView);
        mListener = listener;

        itemView.setOnClickListener(this);

        mAuthorName = (TextView) itemView.findViewById(R.id.author_name);
    }

    void show(String authorName) {
        mAuthorName.setText(authorName);
    }

    @Override
    public void onClick(View view) {
        mListener.onItemSelected(getAdapterPosition());
    }

    interface AuthorsViewHolderEvents {
        void onItemSelected(int position);
    }
}
