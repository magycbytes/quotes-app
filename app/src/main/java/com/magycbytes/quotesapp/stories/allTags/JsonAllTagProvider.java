package com.magycbytes.quotesapp.stories.allTags;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.magycbytes.quotesapp.R;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 09/01/2017.
 */

public class JsonAllTagProvider implements AllTagsProvider {

    private Events mListener;
    private Context mContext;

    public JsonAllTagProvider(Context context) {
        mContext = context;
    }

    @Override
    public void getAllTags() {
        InputStream raw =  mContext.getResources().openRawResource(R.raw.all_tags);
        Reader rd = new BufferedReader(new InputStreamReader(raw));

        List<String> tags = new Gson().fromJson(rd, new TypeToken<ArrayList<String>>(){}.getType());
        mListener.onTagsAvailable(tags);
    }

    @Override
    public void setListener(Events listener) {
        mListener = listener;
    }
}
