package com.magycbytes.quotesapp.stories.authors;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.quotesapp.R;
import com.magycbytes.quotesapp.stories.authorsExtractor.Author;

import java.util.List;

/**
 * Created by Alex on 20/12/2016.
 */

class AuthorsListAdapter extends RecyclerView.Adapter<AuthorsViewHolder> implements AuthorsViewHolder.AuthorsViewHolderEvents {

    private List<Author> mAuthors;
    private AuthorsListAdapterEvents mEventsListener;

    AuthorsListAdapter(List<Author> authors, AuthorsListAdapterEvents eventsListener) {
        mAuthors = authors;
        mEventsListener = eventsListener;
    }

    @Override
    public AuthorsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_author, parent, false);
        return new AuthorsViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(AuthorsViewHolder holder, int position) {
        holder.show(mAuthors.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return mAuthors.size();
    }

    @Override
    public void onItemSelected(int position) {
        mEventsListener.onAuthorSelected(mAuthors.get(position));
    }

    interface AuthorsListAdapterEvents {
        void onAuthorSelected(Author author);
    }
}
