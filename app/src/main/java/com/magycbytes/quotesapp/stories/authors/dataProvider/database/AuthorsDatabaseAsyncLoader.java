package com.magycbytes.quotesapp.stories.authors.dataProvider.database;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.magycbytes.quotesapp.database.AuthorsTable;
import com.magycbytes.quotesapp.stories.authorsExtractor.Author;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 03/01/2017.
 */

class AuthorsDatabaseAsyncLoader extends AsyncTaskLoader<List<Author>> {

    private static final String LOG_TAG = AuthorsDatabaseAsyncLoader.class.getSimpleName();

    private List<Author> mResponse;
    private String mFilter;

    AuthorsDatabaseAsyncLoader(Context context, String filter) {
        super(context);
        mFilter = filter;
    }

    @Override
    public List<Author> loadInBackground() {
        Log.d(LOG_TAG, "Loading authors from database");

        AuthorsTable authorsTable = new AuthorsTable();
        List<Author> authors = authorsTable.getAll();

        if (mFilter != null) {
            List<Author> filteredAuthors = new ArrayList<>();
            String lowerCaseFilter = mFilter.toLowerCase();
            for (Author author : authors) {
                if (author.getName().toLowerCase().contains(lowerCaseFilter)) filteredAuthors.add(author);
            }
            return filteredAuthors;
        }

        return authors;
    }

    @Override
    public void deliverResult(List<Author> data) {
        mResponse = data;

        if (isStarted()) super.deliverResult(data);
    }

    @Override
    protected void onStartLoading() {
        if (mResponse != null) deliverResult(mResponse);
        if (takeContentChanged() || mResponse == null) forceLoad();
    }
}
