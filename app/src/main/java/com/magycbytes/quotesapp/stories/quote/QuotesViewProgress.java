package com.magycbytes.quotesapp.stories.quote;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Created by Alex on 06/01/2017.
 */

public class QuotesViewProgress {

    private static final String LOG_TAG = QuotesViewProgress.class.getSimpleName();

    private Context mContext;

    public QuotesViewProgress(Context context) {
        mContext = context;
    }

    public void saveProgress(String authorLink, int position) {
        Log.d(LOG_TAG, "Saving " + authorLink + " with position " + position);

        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
        editor.putInt(authorLink, position);
        editor.apply();
    }

    public int getProgress(String authorLink) {
        int position = PreferenceManager.getDefaultSharedPreferences(mContext).getInt(authorLink, 0);
        Log.d(LOG_TAG, "Position for " + authorLink + " is " + position);
        return position;
    }
}
