package com.magycbytes.quotesapp.stories.quote;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.magycbytes.quotesapp.stories.quote.fragment.QuoteFragment;
import com.magycbytes.quotesapp.stories.quotesExtractor.Quote;

import java.util.List;

/**
 * Created by Alex on 20/12/2016.
 */

public class QuotesPagerAdapter extends FragmentStatePagerAdapter {

    private List<Quote> mQuoteList;

    public QuotesPagerAdapter(List<Quote> quoteList, FragmentManager fm) {
        super(fm);
        mQuoteList = quoteList;
    }

    @Override
    public int getCount() {
        return mQuoteList.size();
    }


    @Override
    public Fragment getItem(int position) {
        return QuoteFragment.newInstance(mQuoteList.get(position).getText(), getCount(), position, mQuoteList.get(position).getTags());
    }
}
