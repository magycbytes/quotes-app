package com.magycbytes.quotesapp.stories.quotesByTag;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import java.util.List;

/**
 * Created by Alex on 21/12/2016.
 */

class QuotesByTagJsonProvider implements QuotesByTagProvider, LoaderManager.LoaderCallbacks<List<QuoteWithAuthor>> {

    private Events mListener;
    private String mTag;
    private Context mContext;
    private LoaderManager mLoaderManager;

    QuotesByTagJsonProvider(String tag, Context context, LoaderManager loaderManager) {
        mTag = tag;
        mContext = context;
        mLoaderManager = loaderManager;
    }

    @Override
    public void getQuotes() {
        mLoaderManager.initLoader(1, null, this);
    }

    @Override
    public void setEventsListener(Events listener) {
        mListener = listener;
    }

    @Override
    public Loader<List<QuoteWithAuthor>> onCreateLoader(int id, Bundle args) {
        return new QuotesByTagAsyncLoader(mContext, mTag);
    }

    @Override
    public void onLoadFinished(Loader<List<QuoteWithAuthor>> loader, List<QuoteWithAuthor> data) {
        mListener.onQuotesLoaded(data);
    }

    @Override
    public void onLoaderReset(Loader<List<QuoteWithAuthor>> loader) {

    }
}
