package com.magycbytes.quotesapp.stories.allTags;

import java.util.List;

/**
 * Created by Alex on 09/01/2017.
 */

class AllTagsPresenter implements AllTagsProvider.Events {

    private AllTagsProvider mAllTagsProvider;
    private AllTagsView mAllTagsView;

    AllTagsPresenter(AllTagsProvider allTagsProvider, AllTagsView allTagsView) {
        mAllTagsProvider = allTagsProvider;
        mAllTagsView = allTagsView;

        mAllTagsProvider.setListener(this);
    }

    void showAllTags() {
        mAllTagsProvider.getAllTags();
    }

    @Override
    public void onTagsAvailable(List<String> tags) {
        mAllTagsView.show(tags);
    }
}
