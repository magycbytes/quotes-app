package com.magycbytes.quotesapp.stories.newMainMenu;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import com.magycbytes.quotesapp.stories.newMainMenu.quoteProvider.OneQuoteAsyncTaskLoader;

/**
 * Created by alex on 20/07/2017.
 */
public class QuoteOfDayProviderImpl implements QuoteOfDayProvider, LoaderManager.LoaderCallbacks<QuoteViewModel> {

    private Events mEventsListener;
    private Context mContext;
    private LoaderManager mLoaderManager;

    QuoteOfDayProviderImpl(Context context, LoaderManager loaderManager) {
        mContext = context;
        mLoaderManager = loaderManager;
    }

    @Override
    public void loadQuoteOfDay() {
        mLoaderManager.initLoader(100, null, this);
    }

    @Override
    public void loadAnotherQuote() {
        mLoaderManager.restartLoader(101, null, this);
    }

    @Override
    public void setEventsListener(Events eventsListener) {
        mEventsListener = eventsListener;
    }

    @Override
    public Loader<QuoteViewModel> onCreateLoader(int id, Bundle args) {
        return new OneQuoteAsyncTaskLoader(mContext);
    }

    @Override
    public void onLoadFinished(Loader<QuoteViewModel> loader, QuoteViewModel data) {
        mEventsListener.onQuoteOfDayAvailable(data);
    }

    @Override
    public void onLoaderReset(Loader<QuoteViewModel> loader) {

    }
}
