package com.magycbytes.quotesapp.stories.newMainMenu;

/**
 * Created by alex on 20/07/2017.
 */
public interface QuoteOfDayProvider {

    void loadQuoteOfDay();

    void loadAnotherQuote();

    void setEventsListener(Events eventsListener);


    interface Events {
        void onQuoteOfDayAvailable(QuoteViewModel quoteViewModel);
    }
}
