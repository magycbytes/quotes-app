package com.magycbytes.quotesapp.stories.intro;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.magycbytes.quotesapp.R;

public class IntroActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Boolean> {

    private ProgressBar mProgressBar;

    public static void start(Context context) {
        context.startActivity(new Intent(context, IntroActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        mProgressBar = (ProgressBar) findViewById(R.id.deserialization_progress);
        mProgressBar.setVisibility(View.VISIBLE);

        getSupportLoaderManager().initLoader(3, null, this);
    }

    @Override
    public Loader<Boolean> onCreateLoader(int id, Bundle args) {
        return new DeserializatorLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<Boolean> loader, Boolean data) {
        if (data) mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onLoaderReset(Loader<Boolean> loader) {

    }
}
