package com.magycbytes.quotesapp.stories.quotesByTag;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.magycbytes.quotesapp.R;

import java.util.List;

/**
 * Created by Alex on 21/12/2016.
 */

class QuoteByTagListView {

    private RecyclerView mQuoteList;
    private View mLoadProgress;

    QuoteByTagListView(View view) {
        mQuoteList = (RecyclerView) view.findViewById(R.id.quotes_list);
        mLoadProgress = view.findViewById(R.id.load_progress);
    }

    void show(List<QuoteWithAuthor> quotes)  {
        mQuoteList.setLayoutManager(new LinearLayoutManager(mQuoteList.getContext()));
        mQuoteList.setAdapter(new QuoteListAdapter(quotes));
    }

    void showLoadProgress() {
        mLoadProgress.setVisibility(View.VISIBLE);
    }

    void hideLoadProgress() {
        mLoadProgress.setVisibility(View.GONE);
    }
}
