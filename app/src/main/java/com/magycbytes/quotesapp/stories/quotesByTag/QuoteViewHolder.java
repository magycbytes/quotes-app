package com.magycbytes.quotesapp.stories.quotesByTag;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.magycbytes.quotesapp.R;
import com.magycbytes.quotesapp.stories.authorsExtractor.Author;
import com.magycbytes.quotesapp.stories.quote.QuoteActivity;
import com.magycbytes.quotesapp.stories.quotesExtractor.Quote;
import com.magycbytes.quotesapp.util.ClipboardUtil;

/**
 * Created by Alex on 21/12/2016.
 */

class QuoteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView mQuoteText;
    private TextView mAuthorName;
    private Author mAuthor;

    QuoteViewHolder(View itemView) {
        super(itemView);

        itemView.findViewById(R.id.copy_quote).setOnClickListener(this);

        mQuoteText = (TextView) itemView.findViewById(R.id.quote_text);
        mAuthorName = (TextView) itemView.findViewById(R.id.author_name);
        mAuthorName.setOnClickListener(this);
    }

    void show(Author author, Quote quote) {
        mAuthor = author;

        mAuthorName.setText(author.getName());
        mQuoteText.setText(quote.getText());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.author_name:
                QuoteActivity.start(view.getContext(), mAuthor);
                break;
            default:
                ClipboardUtil.copyText(view.getContext(), mQuoteText.getText().toString());
        }
    }
}
