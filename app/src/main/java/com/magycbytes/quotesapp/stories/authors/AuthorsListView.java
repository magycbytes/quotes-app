package com.magycbytes.quotesapp.stories.authors;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.magycbytes.quotesapp.R;
import com.magycbytes.quotesapp.stories.authorsExtractor.Author;
import com.magycbytes.quotesapp.stories.quote.QuoteActivity;

import java.util.List;

/**
 * Created by Alex on 20/12/2016.
 */

class AuthorsListView implements AuthorsListAdapter.AuthorsListAdapterEvents {

    private RecyclerView mAuthorsList;

    AuthorsListView(View view) {
        mAuthorsList = (RecyclerView) view.findViewById(R.id.authors_list);
    }

    void showAuthors(List<Author> allAuthors) {
        mAuthorsList.setLayoutManager(new LinearLayoutManager(mAuthorsList.getContext()));
        mAuthorsList.setAdapter(new AuthorsListAdapter(allAuthors, this));
    }

    @Override
    public void onAuthorSelected(Author author) {
        QuoteActivity.start(mAuthorsList.getContext(), author);
    }
}
