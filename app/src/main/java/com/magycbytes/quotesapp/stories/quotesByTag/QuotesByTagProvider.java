package com.magycbytes.quotesapp.stories.quotesByTag;

import java.util.List;

/**
 * Created by Alex on 21/12/2016.
 */

public interface QuotesByTagProvider {

    void getQuotes();

    void setEventsListener(Events listener);

    interface Events {
        void onQuotesLoaded(List<QuoteWithAuthor> quotes);
    }
}
