package com.magycbytes.quotesapp.stories.quote;

import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.magycbytes.quotesapp.R;
import com.magycbytes.quotesapp.stories.quotesExtractor.Quote;

import java.util.List;

/**
 * Created by Alex on 20/12/2016.
 */

class QuoteView {

    private ViewPager mQuotesContainer;
    private FragmentManager mFragmentManager;
    private View mLoadingProgress;

    QuoteView(View view, FragmentManager fragmentManager) {
        mQuotesContainer = (ViewPager) view.findViewById(R.id.quotes_container);
        mLoadingProgress = view.findViewById(R.id.load_progress);
        mFragmentManager = fragmentManager;
    }

    void show(List<Quote> quotes) {
        mQuotesContainer.setAdapter(new QuotesPagerAdapter(quotes, mFragmentManager));
    }

    void show(List<Quote> quotes, int currentPosition) {
        show(quotes);
        mQuotesContainer.setCurrentItem(currentPosition);
    }

    void showLoadProgress() {
        mLoadingProgress.setVisibility(View.VISIBLE);
    }

    void hideLoadProgress() {
        mLoadingProgress.setVisibility(View.GONE);
    }

    int getCurrentPosition() {
        return mQuotesContainer.getCurrentItem();
    }
}
