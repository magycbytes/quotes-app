package com.magycbytes.quotesapp.stories.allTags;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.magycbytes.quotesapp.R;

public class AllTagsActivity extends AppCompatActivity {

    public static void start(Context context) {
        context.startActivity(new Intent(context, AllTagsActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_tags);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, AllTagsFragment.newInstance())
                .commit();
    }
}
