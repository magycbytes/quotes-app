package com.magycbytes.quotesapp.stories.quote.dataProvider.json;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.magycbytes.quotesapp.stories.quote.dataProvider.commonLogic.AuthorsQuotes;
import com.magycbytes.quotesapp.stories.quote.dataProvider.commonLogic.QuotesAsyncDataProvider;

/**
 * Created by Alex on 20/12/2016.
 */

public class QuotesJsonProvider extends QuotesAsyncDataProvider {


    private String mAuthorLink;

    public QuotesJsonProvider(Context context, String authorLink, LoaderManager loaderManager) {
        super(context, loaderManager, 2);
        mAuthorLink = authorLink;
    }

    @Override
    public Loader<AuthorsQuotes> onCreateLoader(int id, Bundle args) {
        return new QuotesJsonAsyncLoader(mContext, mAuthorLink);
    }
}
