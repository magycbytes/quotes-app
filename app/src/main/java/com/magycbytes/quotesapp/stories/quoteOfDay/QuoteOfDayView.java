package com.magycbytes.quotesapp.stories.quoteOfDay;

import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.magycbytes.quotesapp.R;
import com.magycbytes.quotesapp.stories.quotesExtractor.Quote;

/**
 * Created by Alex on 21/12/2016.
 */

class QuoteOfDayView {

    private TextView mAuthor;
    private TextView mQuoteText;

    QuoteOfDayView(View view) {
        mQuoteText = (TextView) view.findViewById(R.id.quote_text);
        mAuthor = (TextView) view.findViewById(R.id.author_name);
    }

    void showTheQuote(Quote quote, String authorName) {
        mAuthor.setText(authorName);
        mQuoteText.setText(Html.fromHtml(quote.getText()));
    }
}
