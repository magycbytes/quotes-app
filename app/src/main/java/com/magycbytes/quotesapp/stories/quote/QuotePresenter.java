package com.magycbytes.quotesapp.stories.quote;

import android.content.Context;

import com.magycbytes.quotesapp.stories.quote.dataProvider.QuotesProvider;
import com.magycbytes.quotesapp.stories.quote.dataProvider.commonLogic.AuthorsQuotes;

/**
 * Created by Alex on 20/12/2016.
 */

class QuotePresenter implements QuotesProvider.Events {

    private QuotesProvider mQuotesProvider;
    private QuoteView mQuoteView;

    QuotePresenter(QuotesProvider quotesProvider, QuoteView quoteView) {
        mQuotesProvider = quotesProvider;
        mQuoteView = quoteView;

        mQuotesProvider.setListener(this);
    }

    void load() {
        mQuoteView.showLoadProgress();
        mQuotesProvider.getQuotes();
    }

    void saveViewProgress(String authorLink, Context context) {
        QuotesViewProgress progress = new QuotesViewProgress(context);
        progress.saveProgress(authorLink, mQuoteView.getCurrentPosition());
    }

    @Override
    public void onQuotesReady(AuthorsQuotes quotes) {
        mQuoteView.show(quotes.getQuotes(), quotes.getLastPosition());
        mQuoteView.hideLoadProgress();
    }
}
