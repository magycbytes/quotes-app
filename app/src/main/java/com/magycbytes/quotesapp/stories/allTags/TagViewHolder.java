package com.magycbytes.quotesapp.stories.allTags;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.magycbytes.quotesapp.R;
import com.magycbytes.quotesapp.stories.quotesByTag.QuotesByTagActivity;

/**
 * Created by Alex on 05/01/2017.
 */

class TagViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView mTagName;

    TagViewHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        mTagName = (TextView) itemView.findViewById(R.id.tagName);
    }

    void show(String tag) {
        mTagName.setText(tag);
    }

    @Override
    public void onClick(View v) {
        QuotesByTagActivity.start(mTagName.getContext(), mTagName.getText().toString());
    }
}
