package com.magycbytes.quotesapp.stories.quoteOfDay;

import com.magycbytes.quotesapp.stories.authorsExtractor.Author;
import com.magycbytes.quotesapp.stories.quotesExtractor.Quote;

/**
 * Created by Alex on 21/12/2016.
 */

interface QuoteOfDayProvider {

    void getTheQuote();

    void setEventsListener(Events eventsListener);

    interface Events {
        void onQuoteOfDayAvailable(Quote quote, Author author);
    }
}
