package com.magycbytes.quotesapp.stories.authorsExtractor;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.magycbytes.quotesapp.network.ApiManager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class AllAuthorsService extends IntentService {

    public static void start(Context context) {
        context.startService(new Intent(context, AllAuthorsService.class));
    }


    public AllAuthorsService() {
        super("AllAuthorsService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            Response<ResponseBody> response = ApiManager.getApi().getAllAuthors().execute();
            Document document = Jsoup.parse(response.body().byteStream(), null, ApiManager.BASE_URL);
            Elements authors = document.getElementsByClass("bqLn");

            List<Author> extractedAuthors = new ArrayList<>();
            for (Element author : authors) {
                Elements aTags = author.getElementsByTag("a");
                if (aTags.size() < 1) continue;

                Element tag = aTags.get(0);
                String link = tag.attr("href");
                String authorName = tag.text();
                extractedAuthors.add(new Author(authorName, link));
            }

            new Gson().toJson(extractedAuthors);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
