package com.magycbytes.quotesapp.stories.quoteOfDay;

import com.magycbytes.quotesapp.stories.authorsExtractor.Author;
import com.magycbytes.quotesapp.stories.quotesExtractor.Quote;

/**
 * Created by Alex on 21/12/2016.
 */

class QuoteOfDayPresenter implements QuoteOfDayProvider.Events {

    private QuoteOfDayProvider mProvider;
    private QuoteOfDayView mView;

    QuoteOfDayPresenter(QuoteOfDayProvider provider, QuoteOfDayView view) {
        mProvider = provider;
        mView = view;

        mProvider.setEventsListener(this);
    }

    void showTheQuote() {
        mProvider.getTheQuote();
    }

    @Override
    public void onQuoteOfDayAvailable(Quote quote, Author author) {
        mView.showTheQuote(quote, author.getName());
    }
}
