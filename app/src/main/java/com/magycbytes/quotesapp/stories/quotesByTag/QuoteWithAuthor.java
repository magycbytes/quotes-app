package com.magycbytes.quotesapp.stories.quotesByTag;

import com.magycbytes.quotesapp.stories.authorsExtractor.Author;
import com.magycbytes.quotesapp.stories.quotesExtractor.Quote;

/**
 * Created by Alex on 03/01/2017.
 */

public class QuoteWithAuthor {
    private Quote mQuote;
    private Author mAuthor;

    public QuoteWithAuthor() {
    }

    public QuoteWithAuthor(Quote quote, Author author) {
        mQuote = quote;
        mAuthor = author;
    }

    public Quote getQuote() {
        return mQuote;
    }

    public void setQuote(Quote quote) {
        mQuote = quote;
    }

    public Author getAuthor() {
        return mAuthor;
    }

    public void setAuthor(Author author) {
        mAuthor = author;
    }
}
