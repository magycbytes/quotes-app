package com.magycbytes.quotesapp.stories.authors;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.quotesapp.R;
import com.magycbytes.quotesapp.stories.authors.dataProvider.AuthorDataProvider;
import com.magycbytes.quotesapp.stories.authors.dataProvider.database.AuthorsDatabaseProvider;
import com.magycbytes.quotesapp.stories.authors.dataProvider.json.AuthorJsonDataProvider;
import com.magycbytes.quotesapp.util.ApplicationSettings;


public class AuthorsFragment extends Fragment {

    public static final String FILTER = "Filter";

    public static AuthorsFragment newInstance() {
        return new AuthorsFragment();
    }

    public static AuthorsFragment newInstance(String filter) {
        Bundle bundle = new Bundle();
        bundle.putString(FILTER, filter);

        AuthorsFragment authorsFragment = new AuthorsFragment();
        authorsFragment.setArguments(bundle);

        return authorsFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflate = inflater.inflate(R.layout.fragment_authors, container, false);

        AuthorsListView listView = new AuthorsListView(inflate);


        AuthorsListPresenter presenter = new AuthorsListPresenter(getAuthorDataProvider(), listView);
        presenter.load();

        return inflate;
    }

    @NonNull
    private AuthorDataProvider getAuthorDataProvider() {
        String filter = null;
        Bundle arguments = getArguments();
        if (arguments != null && arguments.containsKey(FILTER)) filter = arguments.getString(FILTER);

        ApplicationSettings applicationSettings = new ApplicationSettings(getContext());
        AuthorDataProvider dataProvider;
        if (applicationSettings.isDataDeserialized()) {
            dataProvider = new AuthorsDatabaseProvider(getLoaderManager(), filter, getContext());
        } else {
            dataProvider = new AuthorJsonDataProvider(getContext(), filter, getLoaderManager());
        }
        return new AuthorJsonDataProvider(getContext(), filter, getLoaderManager());
    }

}
