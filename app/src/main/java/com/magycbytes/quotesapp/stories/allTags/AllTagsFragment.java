package com.magycbytes.quotesapp.stories.allTags;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.quotesapp.R;


public class AllTagsFragment extends Fragment {


    public static AllTagsFragment newInstance() {
        return new AllTagsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_tags, container, false);

        AllTagsView tagsView = new AllTagsView(view);
        AllTagsPresenter presenter = new AllTagsPresenter(new JsonAllTagProvider(getContext()), tagsView);
        presenter.showAllTags();

        return view;
    }

}
