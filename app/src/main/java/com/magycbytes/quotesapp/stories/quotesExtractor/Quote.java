package com.magycbytes.quotesapp.stories.quotesExtractor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 20/12/2016.
 */

public class Quote {

    private String mText;
    private String mLink;
    private String mAuthorLink;
    private List<String> mTags = new ArrayList<>();
    private int mId;
    private long mAuthorId;

    public Quote() {}

    public Quote(String text, long authorId, List<String> tags) {
        mText = text;
        mAuthorId = authorId;
        mTags = tags;
    }

    public Quote(String text, String link, String authorLink) {
        mText = text;
        mLink = link;
        mAuthorLink = authorLink;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    public String getLink() {
        return mLink;
    }

    public void setLink(String link) {
        mLink = link;
    }

    public String getAuthorLink() {
        return mAuthorLink;
    }

    public void setAuthorLink(String authorLink) {
        mAuthorLink = authorLink;
    }

    public void addTag(String tag) {
        mTags.add(tag);
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public List<String> getTags() {
        return mTags;
    }
}
