package com.magycbytes.quotesapp.stories.newMainMenu;

import android.view.View;
import android.widget.TextView;
import com.magycbytes.quotesapp.R;
import com.magycbytes.quotesapp.stories.quotesExtractor.Quote;

/**
 * Created by alex on 20/07/2017.
 */
public class QuoteView {

    private TextView mText;
    private TextView mAuthor;

    QuoteView(View view) {
        mText = (TextView) view.findViewById(R.id.quoteText);
        mAuthor = (TextView) view.findViewById(R.id.authorName);
    }

    void show(String quoteText, String quoteAuthor) {
        mText.setText(quoteText);
        mAuthor.setText(quoteAuthor);
    }
}
