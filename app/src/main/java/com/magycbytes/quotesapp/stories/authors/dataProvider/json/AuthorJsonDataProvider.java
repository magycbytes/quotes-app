package com.magycbytes.quotesapp.stories.authors.dataProvider.json;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.magycbytes.quotesapp.stories.authors.dataProvider.commonLogic.AuthorsAsyncDataProvider;
import com.magycbytes.quotesapp.stories.authorsExtractor.Author;

import java.util.List;

/**
 * Created by Alex on 20/12/2016.
 */

public class AuthorJsonDataProvider extends AuthorsAsyncDataProvider {

    public AuthorJsonDataProvider(Context context, String filter, LoaderManager loaderManager) {
        super(loaderManager, filter, context, 2);
    }

    @Override
    public Loader<List<Author>> onCreateLoader(int id, Bundle args) {
        return new AuthorsJsonAsyncLoader(mContext, mFilter);
    }
}
