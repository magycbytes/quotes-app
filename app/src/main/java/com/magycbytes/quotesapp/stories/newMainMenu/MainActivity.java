package com.magycbytes.quotesapp.stories.newMainMenu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.magycbytes.quotesapp.R;
import com.magycbytes.quotesapp.stories.quotesExtractor.Quote;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static List<Quote> sQuoteList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainView mainView = new MainViewImpl(findViewById(R.id.activity_main));
        QuoteOfDayProvider dataProvider = new QuoteOfDayProviderImpl(this, getSupportLoaderManager());
        MainPresenter presenter = new MainPresenter(mainView, dataProvider, new MainButtonClickExecutorImpl(this));
        presenter.showQuoteOfDay();
    }
}
